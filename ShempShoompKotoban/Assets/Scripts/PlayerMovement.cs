﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed = 15f;
    public bool moving;
    public bool left;
    private float hInput;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        movementInput();
        movementDirection();
    }

    void movementDirection()
    {
        if (hInput < 0 && !left)
        {
            left = true;
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (hInput > 0 && left)
        {
            left = false;
            GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    void movementInput()
    {
        hInput = Input.GetAxisRaw("Horizontal");
        transform.position += hInput * speed * Time.deltaTime * Vector3.right;
        if (hInput != 0 && !moving)
        {
            moving = true;
            GetComponent<Animator>().SetTrigger("run");
        }
        else if (hInput == 0 && moving)
        {
            moving = false;
            GetComponent<Animator>().SetTrigger("idle");
        }
    }
}
